import { createContext } from 'react';

export interface AppContextInterface {
    showModal: (message: JSX.Element) => void,
    showResult: (message: JSX.Element) => void,
}

const AppContext = createContext<AppContextInterface>({
    showModal: () => {},
    showResult: () => {},
});

export default AppContext;