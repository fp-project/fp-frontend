import React from 'react'
import { Link } from 'react-router-dom'

const NotFound: React.FC = () => {
    return (
        <div>
            <h1>Seems you're lost.</h1>
            <Link to="/">Back to home</Link>
        </div>
    );
}

export default NotFound;