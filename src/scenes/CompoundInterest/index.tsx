import React, { useReducer, useContext} from 'react';
import useForm from 'react-hook-form';
import "./style.scss"

import{
    Field,
    Gap,
    Button,
} from "components"
import AppContext from 'contexts/AppContext';

const dayToMonths = (time: number) => {
    return time / 30
}

const monthsToYears = (time: number) => {
    return time / 12
}

const convertToYears = (time: number, timeType:number) =>{
    switch(timeType){
        case 0:
            return time
        case 1:
            return monthsToYears(time)
        case 2:
            return monthsToYears(dayToMonths(time))
        default:
            throw new Error("Wrong Input, make sure you choose the right input -ctd")
    }
}

const mapCompoundToN = (compoundtype: number) => {
    switch(compoundtype){
        case 0:
            return 1 //
        case 1:
            return 2
        case 2:
            return 4
        case 3:
            return 12
        case 4:
            return 365
            default:
                    throw new Error("Wrong Input, make sure you choose the right input -mctn")
    }
}

interface CompoundInterestInput {
    principal: number,
    interestRate: number,
    time: number,
    n:number,

}

interface FormAction {
    type: string,
    data: any,
}

const formManipulator = (state: any, action: FormAction) =>{
    switch(action.type){
        case "UPDATE":
            return{...state, ...action.data}
    }
}

// var stringify = null








const CompoundInterest: React.FC = () => {
    const appContext = useContext(AppContext);
    const [form, dispatch] = useReducer(formManipulator,{
        principal: 0,
        interestRate: 0,
        time: 0,
        n: 0,
    })

    const calculateCompound = (record: CompoundInterestInput) => {
        const stringify = record
        console.log("json stringify"+" "+JSON.stringify(stringify))

        fetch("https://b5816fmvl6.execute-api.us-east-1.amazonaws.com/production/compoundcalculator", {
            method: 'POST',
            body: JSON.stringify(record),
            headers: {
                'Access-Control-Allow-Origin': '*',
                'X-Api-Key': 'AOdlUr6domaVJkxBxeyOu7kt5jF7UtJi32AmivqB',
                'Content-Type': 'application/json',
            },
        })
            .then(res => res.json())
            .then(result => {
                console.log(result);
                appContext.showResult(
                    <p>{result}</p>
                );
            });

    }

    const { register, errors } = useForm();




     return (
        <form className="l-simple-interest__form">
            <div className="l-simple-interest__body">
                <Gap gap={42} axis="vertical" />
                <Field 
                    name="PRINCIPAL"
                    type="number"
                    info="The amount of money you want to put into a bank"
                    placeholder="PRINCIPAL"
                    // updateValue={() => {}}
                    updateValue={(value) => {
                        dispatch({type: "UPDATE", data: {principal: value}});
                    }}
                    scale={1}
                    formRef={register({
                        required: true,
                    })}
                    error={errors.principal}
                />
                <Gap gap={32} axis="vertical" />
                <div className="h-row">
                    <div
                        style={{
                            flex: "1 1 auto"
                        }}
                    >
                        <Field 
                            name="INTEREST RATE (%)"
                            type="number"
                            info="Percentage of principal that bank is paying you"
                            placeholder="INTEREST RATE"
                            updateValue={(value) => {
                                dispatch({type: "UPDATE", data: {interestRate: value}});
                            }}
                            scale={1}
                            formRef={register({
                                required: true,
                            })}
                            error={errors.rate}
                        />
                    </div>
                
                    <Gap gap={32} axis="horizontal" />
                    {/* <div
                        style={{
                            width: "30%",
                        }}
                    >
                        <Field 
                            name="TIME TYPE"
                            type="text"
                            updateValue={(value) => dispatch({type: "UPDATE", data: {interestTimeType: value}})}
                            scale={1}
                            dropdownList={[
                                [0, "DAY"],
                                [1, "MONTH"],
                                [2, "YEAR"],
                            ]}
                            formRef={register({
                                required: true,
                            })}
                            error={errors.time}
                        />
                    </div> */}
                </div>
                <Gap gap={32} axis="vertical" />
                <div className="h-row">
                    <div
                        style={{
                            width: "70%",
                        }}
                    >
                        <Field 
                            name="TIME"
                            type="number"
                            updateValue={(value) => dispatch({type: "UPDATE", data: {time: value}})}
                            info="How much time the money will stay into a bank"
                            scale={1}
                            placeholder="TIME"
                            formRef={register({
                                required: true,
                            })}
                            error={errors.time}
                        />
                    </div>
                    <Gap gap={24} axis="horizontal" />
                    <div
                        style={{
                            width: "30%",
                        }}
                    >
                        <Field 
                            name="TIME TYPE"
                            type="text"
                            updateValue={(value) => dispatch({type: "UPDATE", data: {timeType: value}})}
                            scale={1}
                            dropdownList={[
                                [0, "YEAR"],
                                [1, "MONTH"],
                                [2, "DAY"],
                            ]}
                            formRef={register({
                                required: true,
                            })}
                            error={errors.time}
                        />
                    </div>
                </div>
                <Gap gap={32} axis="vertical" />

                <Field 
                    name="COMPOUND"
                    type="text"
                    updateValue={(value) => dispatch({type: "UPDATE", data: {compoundType: value}})}
                    scale={1}
                    dropdownList={[
                        [0, "YEARLY"],
                        [1, "HALF YEARLY"],
                        [2, "QUARTERLY"],
                        [3, "MONTHLY"],
                        [4, "DAILY"],
                    ]}
                    formRef={register({
                        required: true,
                    })}
                    error={errors.principal}
                />



                <Gap gap={64} axis="vertical" />
                <span 
                    style={{
                        flex: 1,
                    }}
                />
            </div>
            <Button name="CALCULATE" do={() => {
                
                
                console.log("interest time type"+" "+form.interestTimeType)
                console.log("interestRate"+" "+form.interestRate)
                console.log("time type"+" "+ form.timeType)
                console.log( "time"+" "+form.time)
                console.log("compound type"+" "+form.compoundType)
                
                

                const interestRate = parseInt(form.interest) / convertToYears(1, form.timeType);
                const time = convertToYears(parseInt(form.time), form.timeType);
                const compound = mapCompoundToN(form.compoundType)
                console.log("compound "+compound)
                calculateCompound({
                    principal : parseInt(form.principal),
                    interestRate: parseInt(form.interestRate),
                    time : time,
                    n: compound,

                })
                // calculateInterest({
                //     savings: form.savings,
                //     interest,
                //     time,
                // });
            }}/>
        </form>
    );

}

export default CompoundInterest;