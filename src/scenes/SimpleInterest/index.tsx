import React, { useReducer, useContext } from 'react';
import useForm from 'react-hook-form';
import "./style.scss"

import {
    Field,
    Gap,
    Button,
} from "components";

import AppContext from 'contexts/AppContext';


const yearsToMonths = (duration: number) => {
    return 12 * duration;
}

const monthsToDays = (duration: number) => {
    return 30 * duration;
}

const convertToDays = (duration: number, timeType: number) => {
    switch(timeType) {
        case 0:
            return duration;
        case 1:
            return monthsToDays(duration);
        case 2:
            return monthsToDays(yearsToMonths(duration));
        default:
            throw new Error("Time type only available for 1 - 2");    
    }
}

interface SimpleInterestInput {
    savings: number,
    interest: number,
    duration: number,
}

interface FormAction {
    type: string,
    data: any,
}

const formManipulator = (state: any, action: FormAction) => {
    switch(action.type) {
        case "UPDATE":
            return {...state, ...action.data};
    }
}

const SimpleInterest: React.FC = () => {
    const appContext = useContext(AppContext);
    const [form, dispatch] = useReducer(formManipulator, {
        savings: 0,
        interest: 0,
        duration: 0,
    });

    const calculateInterest = (record: SimpleInterestInput) => {
        appContext.showResult(
            <span><b>Calculating...</b></span>
        )
        fetch("https://b5816fmvl6.execute-api.us-east-1.amazonaws.com/production/simpleinterestcalculator/", {
            method: 'POST',
            body: JSON.stringify(record),
            headers: {
                'Access-Control-Allow-Origin': '*',
                'X-Api-Key': 'AOdlUr6domaVJkxBxeyOu7kt5jF7UtJi32AmivqB',
                'Content-Type': 'application/json',
            },
        })
            .then(res => res.json())
            .then(result => {
                appContext.showResult(
                    <p>The amount is <b>{result}</b></p>
                );
            });

    }
    const { register, errors } = useForm();
    return (
        <form className="l-simple-interest__form">
            <div className="l-simple-interest__body">
                <Gap gap={42} axis="vertical" />
                <Field 
                    name="PRINCIPAL"
                    type="number"
                    info="The amount of money you want to put into a bank"
                    placeholder="PRINCIPAL"
                    updateValue={(value) => {
                        dispatch({type: "UPDATE", data: {savings: value}});
                    }}
                    scale={1}
                    formRef={register({
                        required: true,
                    })}
                    error={errors.principal}
                />
                <Gap gap={32} axis="vertical" />
                <div className="h-row">
                    <div
                        style={{
                            flex: "1 1 auto"
                        }}
                    >
                        <Field 
                            name="INTEREST RATE (%)"
                            type="number"
                            info="Percentage of principal that bank is paying you"
                            placeholder="INTEREST RATE"
                            updateValue={(value) => {
                                dispatch({type: "UPDATE", data: {interest: value}});
                            }}
                            scale={1}
                            formRef={register({
                                required: true,
                            })}
                            error={errors.rate}
                        />
                    </div>
                    <Gap gap={24} axis="horizontal" />
                    <span style={{
                        fontSize: "12px",
                    }}>
                        PER
                    </span>
                    <Gap gap={24} axis="horizontal" />
                    <div
                        style={{
                            width: "30%",
                        }}
                    >
                        <Field 
                            name="TIME TYPE"
                            type="text"
                            updateValue={(value) => dispatch({type: "UPDATE", data: {interestTimeType: value}})}
                            scale={1}
                            dropdownList={[
                                [0, "DAY"],
                                [1, "MONTH"],
                                [2, "YEAR"],
                            ]}
                            formRef={register({
                                required: true,
                            })}
                            error={errors.time}
                        />
                    </div>
                </div>
                <Gap gap={32} axis="vertical" />
                <div className="h-row">
                    <div
                        style={{
                            width: "70%",
                        }}
                    >
                        <Field 
                            name="DURATION"
                            type="number"
                            updateValue={(value) => dispatch({type: "UPDATE", data: {duration: value}})}
                            info="How much time the money will stay into a bank"
                            scale={1}
                            placeholder="DURATION"
                            formRef={register({
                                required: true,
                            })}
                            error={errors.time}
                        />
                    </div>
                    <Gap gap={24} axis="horizontal" />
                    <div
                        style={{
                            width: "30%",
                        }}
                    >
                        <Field 
                            name="TIME TYPE"
                            type="text"
                            updateValue={(value) => dispatch({type: "UPDATE", data: {durationTimeType: value}})}
                            scale={1}
                            dropdownList={[
                                [0, "DAY"],
                                [1, "MONTH"],
                                [2, "YEAR"],
                            ]}
                            formRef={register({
                                required: true,
                            })}
                            error={errors.time}
                        />
                    </div>
                </div>
                <Gap gap={64} axis="vertical" />
                <span 
                    style={{
                        flex: 1,
                    }}
                />
            </div>
            <Button name="CALCULATE" do={() => {
                const interest = parseFloat(form.interest) / convertToDays(1, form.interestTimeType);
                const duration = convertToDays(parseFloat(form.duration), form.durationTimeType);
                calculateInterest({
                    savings: parseFloat(form.savings),
                    interest,
                    duration,
                });
            }}/>
        </form>
    );
}

export default SimpleInterest;