import React, {useContext} from 'react';
import useForm from 'react-hook-form';

import {
    Field,
    Gap,
} from "components";
import AppContext from "../../contexts/AppContext";


const Annuity: React.FC = () => {
    const { register, handleSubmit, watch, errors } = useForm();
    const appContext = useContext(AppContext);

    function fetchAnnuity(opts: any) {
        fetch('https://b5816fmvl6.execute-api.us-east-1.amazonaws.com/production/annuitycalculator', {
            method: 'post',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'x-api-key': 'AOdlUr6domaVJkxBxeyOu7kt5jF7UtJi32AmivqB',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(opts)
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            console.log(data);
            appContext.showResult(<h3>Withdrawal every end of months, value: {data.result}</h3>)

        });
    }

    const onSubmit = (data: Record<string, any>) => {
        console.log(data);
        data['initialDeposit'] = parseInt(data['initialDeposit']);
        data['interestRate'] = parseInt(data['interestRate']);
        data['time'] = parseInt(data['time']);
        data['yearly'] = (data['yearly'] === "Years");
        console.log(JSON.stringify(data));
        fetchAnnuity(data);
    };
    return (
        <form className="l-annuity-calculator__form" onSubmit={handleSubmit(onSubmit)}>
            <div className="l-annuity-calculator__body">
                <h1>Annuity Calculator</h1>
                <Gap gap={42} axis="vertical" />
                <Field name={'initialDeposit'} type={"number"} placeholder={"0"}
                       scale={1} updateValue={() => {}} formRef={register({required: true})} error={errors.initial} />
                <Gap gap={32} axis="vertical" />
                <div className="h-flex--row-nowrap h-flex--center-center">
                    <div
                        style={{
                            flex: "1 1 auto"
                        }}
                    >
                        <Field name={"interestRate"} type={"number"} placeholder={"0"} scale={1} updateValue={() => {}}
                               formRef={register({required: true, min: 1, max: 100})} error={errors.rate} />
                    </div>
                    <Gap gap={24} axis="horizontal" />
                    <span style={{
                        fontSize: "18px",
                    }}>
                        % per year
                    </span>
                </div>
                <Gap axis="vertical" gap={32}/>
                <div className="h-flex--row-nowrap h-flex--center-center">
                    <div style={{
                        flex: "1 1 auto"
                    }}>
                        <Field name={"time"} type={"number"} placeholder={"1"} scale={1} updateValue={() => {}}
                               formRef={register({required: true})} error={errors.time} />
                    </div>
                    <Gap axis="horizontal" gap={24}/>
                    <Field name="yearly" type={"text"} placeholder={""} scale={1} updateValue={() => {}}
                           formRef={register({required: true})} error={errors.timeType} dropdownList={[
                        [false,"Months"],
                        [true, "Years"]
                    ]} />
                </div>
            </div>
            <input type="submit" name="CALCULATE"/>
        </form>
    );
};

export default Annuity;