import React from 'react'
import { Link } from 'react-router-dom'

// TODO: restyle
const Home: React.FC = () => {
  return (
    <div>
      <h1>Welcome to the Finance Calculator!</h1>
      <ul>
        <li>
          <Link to="simple-interest">Simple Interest Calculator</Link>
        </li>
        <li>
          <Link to="compound-interest">Compound Interest Calculator</Link>
        </li>
        <li>
          <Link to="annuity">Annuity Calculator</Link>
        </li>
        <li>
          <Link to="amortization">Amortization Calculator</Link>
        </li>
        <li>
          <Link to="mortgage">Mortgage Calculator</Link>
        </li>
      </ul>
    </div>
  )
}

export default Home