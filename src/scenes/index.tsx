import NotFound from './NotFound';
import Home from './Home';
import SimpleInterest from './SimpleInterest';
import CompoundInterest from './CompoundInterest';
import Annuity from './Annuity';
import Amortization from './Amortization';
import Mortgage from './Mortgage';

export {
    NotFound,
    Home,
    SimpleInterest,
    CompoundInterest,
    Annuity,
    Amortization,
    Mortgage,
}