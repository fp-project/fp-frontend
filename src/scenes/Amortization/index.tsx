import React, { useContext } from 'react';
import useForm from 'react-hook-form';

import { Gap, Field, Button } from 'components';
import AppContext from 'contexts/AppContext';

interface amortizationInput {
    loanAmount: number
    loanTerm: number
    interestRate: number
}

const Amortization: React.FC = () => {
    const { handleSubmit, register, errors } = useForm();
    const appContext = useContext(AppContext);


    const onSubmit = async (formData: Record<string, any>) => {
        const amortizationRecord: amortizationInput = {
            loanAmount: parseFloat(formData.loanAmount),
            loanTerm: formData.timeType == "Months" ? parseInt(formData.loanTerm) : 12 * parseInt(formData.loanTerm),
            interestRate: parseFloat(formData.interestRate) / 100
        }

        const options = {
            method: 'POST',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'x-api-key': 'AOdlUr6domaVJkxBxeyOu7kt5jF7UtJi32AmivqB',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(amortizationRecord)
        };

        const response = await fetch(
            `https://b5816fmvl6.execute-api.us-east-1.amazonaws.com/production/amortizationcalculator`,
            options
        );
        const result = await response.json();

        appContext.showResult(
            <p>Monthly Payment Amount: {result}</p>
        );
    }

    return (
        <form className="l-amortziation-calculator__form" onSubmit={handleSubmit(onSubmit)}>
            <div className="l-amortization-calculator__body">
                <h2>Amortization Calculator</h2>
                <Gap axis="vertical" gap={42}></Gap>
                <Field
                    name="loanAmount"
                    type="number"
                    scale={1}
                    updateValue={() => {}}
                    placeholder="Loan amount"
                    info="The amount of money you want to borrow from the bank"
                    formRef={register({
                        required: true,
                    })}
                    error={errors.amount}
                    >
                </Field>
                <Gap axis="vertical" gap={32}></Gap>
                <div className="h-row">
                    <div style={{
                        flex: "1 1 auto",
                    }}>
                        <Field
                            name="loanTerm"
                            type="number"
                            scale={1}
                            updateValue={() => {}}
                            placeholder="Loan term"
                            info="How long the loan will exist"
                            formRef={register({
                                required: true,
                            })}
                            error={errors.time}
                            >
                        </Field>
                    </div>
                    <Gap axis="horizontal" gap={16}></Gap>
                    <div>
                        <Field
                            name="timeType"
                            type="text"
                            scale={1}
                            updateValue={() => {}}
                            info="Months/Years"
                            dropdownList={[
                                [0, "Months"],
                                [1, "Years"],
                            ]}
                            formRef={register({
                                required: true,
                            })}
                            error={errors.term}
                            >
                        </Field>
                    </div>
                </div>
                <Gap axis="vertical" gap={32}></Gap>
                <div className="h-row">
                    <div style={{
                        flex: "1 1 auto",
                    }}>
                        <Field
                            name="interestRate"
                            type="number"
                            scale={1}
                            updateValue={() => {}}
                            placeholder="Interest rate"
                            info="A rate which is charged for the use of money"
                            formRef={register({
                                required: true,
                            })}
                            error={errors.rate}
                        >
                        </Field>
                    </div>
                    <Gap axis="horizontal" gap={8}></Gap>
                    <span>
                        <Gap axis="vertical" gap={24}></Gap>
                        % per year
                    </span>
                </div>
                <Gap axis="vertical" gap={32}></Gap>
                <Button name="CALCULATE" type="submit" do={() => {handleSubmit(onSubmit)}}></Button>
            </div>
        </form>
    );
}

export default Amortization;