import React, { useContext, useReducer } from 'react';
import useForm from 'react-hook-form';
import { Field, Gap, Button } from 'components';
import AppContext from 'contexts/AppContext';
import './style.scss';

interface MortgageInput {
    principal: number
    rate: number
    numOfPayments: number
}

type YearOrMonth = 'annually' | 'monthly';

interface MortgageFormState {
    rateType: YearOrMonth
    paymentBasis: YearOrMonth
}

interface FormAction {
    type: string
    payload: YearOrMonth
}

const dropdownOptions = [
    ["annually", "Annually"],
    ["monthly", "Monthly"]
];

const formatThousands = (num: number) => num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');

const reducer = (state: MortgageFormState, action: FormAction) => {
    console.log(action)
    switch (action.type) {
        case 'setRateType':
            return {
                ...state,
                rateType: action.payload
            }
        case 'setPaymentBasis':
            return {
                ...state,
                paymentBasis: action.payload
            }
        default:
            return state;
    }
}


const Mortgage: React.FC = () => {
    const { handleSubmit, register, errors } = useForm();
    const appContext = useContext(AppContext);
    const [state, dispatch] = useReducer(reducer, {
        rateType: 'annually',
        paymentBasis: 'annually'
    });
    
    const setRateType = (payload: YearOrMonth) => dispatch({ type: 'setRateType', payload })
    const setPaymentBasis = (payload: YearOrMonth) => dispatch({ type: 'setPaymentBasis', payload })

    const submitData = () => handleSubmit(onSubmit);

    const onSubmit = async (values: Record<string, any>) => {
        console.log('state', state);
        const mortgageRecord: MortgageInput = {
            principal: parseFloat(values.principal),
            rate: parseFloat(values.rate) / (state.rateType === 'annually' ? 12 : 1),
            numOfPayments: parseFloat(values.numOfPayments) * (state.paymentBasis === 'annually' ? 12 : 1),
        }

        console.log(mortgageRecord);

        const options = {
            method: 'POST',
            body: JSON.stringify(mortgageRecord),
            headers: {
                'Access-Control-Allow-Origin': '*',
                'x-api-key': 'AOdlUr6domaVJkxBxeyOu7kt5jF7UtJi32AmivqB',
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(
            `https://b5816fmvl6.execute-api.us-east-1.amazonaws.com/production/mortgagecalculator`,
            options
        );
        const data = await response.json();

        appContext.showResult(
            <p>Monthly payment for your mortgage: {formatThousands(data)}</p>
        );
    };

    return (
        <form className="l-mortgage-calculator__form" onSubmit={handleSubmit(onSubmit)}>
            <div className="l-mortgage-calculator__body">
                <Gap gap={42} axis="vertical" />
                <Field
                    info='Principal value of the mortgage'
                    type='text'
                    placeholder='Principal'
                    scale={1}
                    updateValue={() => {}}
                    formRef={register({
                        required: 'Required',
                        pattern: {
                            value: /^[+-]?\d+(\.\d+)?$/,
                            message: 'Enter decimal value only',
                        }
                    })}
                    name="principal"
                    error={errors.principal}
                />
                {errors.principal && errors.principal.message}
                <Gap gap={42} axis="vertical" />
                <div className="h-row">
                    <div
                        style={{
                            flex: "1 1 auto"
                        }}
                    >
                        <Field
                            type='text'
                            info='Yearly interest rate of the mortgage, in decimal (10% is 0.1)'
                            placeholder='Interest Rate'
                            scale={1}
                            updateValue={() => {}}
                            formRef={register({
                                required: 'Required',
                                pattern: {
                                    value: /^[+-]?\d+(\.\d+)?$/,
                                    message: 'Enter decimal value only',
                                }
                            })}
                            name="rate"
                            error={errors.rate}
                        />
                    </div>
                    <Gap gap={24} axis="horizontal" />
                    <span style={{
                        fontSize: "12px",
                    }}>
                        PER
                    </span>
                    <Gap gap={24} axis="horizontal" />
                    <div
                        style={{
                            width: "30%",
                        }}
                    >
                        <Field 
                            name="rateType"
                            type="text"
                            updateValue={setRateType}
                            scale={1}
                            dropdownList={dropdownOptions}
                            formRef={register({
                                required: true,
                            })}
                            error={errors.rateType}
                        />
                    </div>
                </div>
                {errors.rate && errors.rate.message}
                <Gap gap={42} axis="vertical" />
                <div className="h-row">
                    <div
                        style={{
                            flex: "1 1 auto"
                        }}
                    >
                        <Field
                            type='text'
                            info='Number of payments, in months'
                            placeholder='Number of Payments'
                            scale={1}
                            updateValue={() => {}}
                            formRef={register({
                                required: 'Required',
                                pattern: {
                                    value: /[0-9]+/,
                                    message: 'Enter number only',
                                }
                            })}
                            name="numOfPayments"
                            error={errors.numOfPayments}
                        />
                    </div>
                    <Gap gap={24} axis="horizontal" />
                    <span style={{
                        fontSize: "12px",
                    }}>
                        PER
                    </span>
                    <Gap gap={24} axis="horizontal" />
                    <div
                        style={{
                            width: "30%",
                        }}
                    >
                        <Field 
                            name="paymentTime"
                            type="text"
                            updateValue={setPaymentBasis}
                            scale={1}
                            dropdownList={dropdownOptions}
                            formRef={register({
                                required: true,
                            })}
                            error={errors.paymentTime}
                        />
                    </div>
                </div>
                {errors.numOfPayments && errors.numOfPayments.message}
                <Gap gap={42} axis="vertical" />
                <Button name="CALCULATE" type="submit" do={submitData}/>
            </div>
        </form>
    );
}

export default Mortgage;
