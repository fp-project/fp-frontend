import React, { useState } from 'react';
import './style.scss';

import NavbarLine from './components/NavbarLine';
import NavbarLabel from './components/NavbarLabel';

type Label = {
    name: string,
    to: string,
}

const navbarLabels: Label[] = [
    {
        name: "SIMPLE INTEREST",
        to: "/simple-interest",
    },
    {
        name: "COMPOUND INTEREST",
        to: "/compound-interest",
    },
    {
        name: "AMORTIZATION",
        to: "/amortization",
    },
    {
        name: "ANNUITY",
        to: "/annuity",
    },
    {
        name: "MORTGAGE",
        to: "/mortgage",
    },
]

const BIG_CIRCLE_RADIUS = 40;
const SMALL_CIRCLE_RADIUS = 20;

const Navbar: React.FC = () => {
    const [currentLabelIndex, setCurrentLabelIndex] = useState(0);
    return (
        <div id="l-navbar">
            {navbarLabels.map((label: Label , index: number) => {
                const components = [
                    <div 
                        key={2 * index}
                        style={{
                            margin: `0 ${currentLabelIndex === index? 0: (BIG_CIRCLE_RADIUS - SMALL_CIRCLE_RADIUS) / 2}px 0 0`,
                        }}
                        className="h-animation"
                    >
                        <NavbarLabel 
                            name={label.name}
                            to={label.to}
                            circleRadius={currentLabelIndex === index? BIG_CIRCLE_RADIUS: SMALL_CIRCLE_RADIUS}
                            hasHoleCircle={currentLabelIndex !== index}
                            textOpacity={currentLabelIndex === index? 1: .2}
                            onClick={() => setCurrentLabelIndex(index)}
                        />
                    </div>,
                ]
                if (index + 1 < navbarLabels.length) {
                    components.push(
                        <div 
                            key={2 * index + 1}
                            style={{
                                margin: `0 ${BIG_CIRCLE_RADIUS / 2}px 0 0`,
                            }}
                            className="h-animation"
                        >
                            <NavbarLine />
                        </div>
                    );
                }

                return components;
            })}
        </div>
    );
}

export default Navbar;
