import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss';

import Gap from 'components/Gap';

type NavbarLabel = {
	name: string,
	to: string,
	circleRadius: number,
	hasHoleCircle: boolean,
	textOpacity: number,
	onClick: (name: string) => void, 
}

const NavbarLabel: React.FC<NavbarLabel> = ({
	name,
	to,
	circleRadius,
	hasHoleCircle,
	textOpacity,
	onClick,
}: NavbarLabel) => {
	return (
		<Link className="l-navbar-label" to={to} onClick={() => onClick(name)}>
			<span 
				className="c-navbar-label__name h-animation"
				style={{
					opacity: textOpacity,
				}}
			>
				{name}
			</span>
			<Gap gap={16} axis="horizontal" />
			<div 
				className="c-navbar-label__circle h-animation"
				style={{
					backgroundColor: hasHoleCircle? "transparent": "white",
					height: circleRadius,
					width: circleRadius,
				}}
			/>
		</Link>
	);
}

export default NavbarLabel;
