import React from 'react';
import './style.scss';

const NavbarLine: React.FC = () => {
  return (
    <div className="c-navbar-line" />
  );
}

export default NavbarLine;
