import React, { useState } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import './style.scss';

import {
	NotFound,
	Home,
	SimpleInterest,
	Annuity,
	Amortization,
	Mortgage,
	CompoundInterest,
} from 'scenes';

import {
	Gap
} from 'components';

import AppContext from 'contexts/AppContext';

import Navbar from './components/Navbar';
const App: React.FC = () => {
	const [modalAppears, setModalAppears] = useState(false);
	const [modalMessage, setModalMessage] = useState(<span />);
	const [result, setResult] = useState(<span />);
	
	const showModal = (message: JSX.Element) => {
		setModalAppears(true);
		setModalMessage(message);
	}

	const showResult = (message: JSX.Element) => {
		setResult(message);
	}

	const contextValue = {
		showModal,
		showResult,
	}

  	return (
		<AppContext.Provider value={contextValue}>
			<div id="l-app">
				<h1 id="l-app__title">
					FINANCIAL <span style={{fontWeight: "normal"}}>CALCULATOR</span>
				</h1>
				<Gap gap={64} axis="vertical" />
				<div id="l-app__body">
					<BrowserRouter>
						<Navbar />
						<Gap gap={64} axis="horizontal" />
						<div id="l-app__form" className="l-app__box">
							<Switch>
								<Route exact path="/" component={SimpleInterest} />
								<Route exact path="/simple-interest" component={SimpleInterest} />
								<Route exact path="/annuity" component={Annuity} />
								<Route exact path="/amortization" component={Amortization} />
								<Route exact path="/mortgage" component={Mortgage} />
								<Route exact path="/compound-interest" component={CompoundInterest} />
								<Route component={NotFound} />
							</Switch>
						</div>
						<Gap gap={64} axis="horizontal" />
						<div id="l-app__result" className="l-app__box">
							{result}
						</div>
					</BrowserRouter>
				</div>
				<div
					id="l-app__modal"
					className='h-animation'
					data-display={modalAppears? "appear": "hide"}
					onClick={() => setModalAppears(false)}
				>
					{modalMessage}
				</div>
			</div>
		</AppContext.Provider>
  	);
}

export default App;
