import React from 'react';

type GapProps = {
    axis: string,
    gap: number,
}

const Gap: React.FC<GapProps> = ({ axis, gap }: GapProps) => {
    const isVertical = axis === 'vertical';

    return (
        <div 
            style={{
                display: "inline-box",
                padding: `${isVertical? gap / 2 : 0}px ${isVertical? 0 : gap / 2}px`,
            }}
        />
    )
}

export default Gap;