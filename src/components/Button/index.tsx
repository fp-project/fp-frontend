import React from 'react'
import "./style.scss";

type ButtonProps = {
	name: string,
	clickable?: boolean,
	width?: string,
	scale?: number,
	iconUrl?: string,
	textColor?: string,
	backColor?: string,
	fontSize?: number,
    fontWeight?: number,
    type?: "button" | "submit" | "reset" | undefined,
	do: () => void,
}

const Button: React.FC<ButtonProps> = (props: ButtonProps) => {
	const scale = props.scale || 1;
	const clickable = props.clickable || true;
	const width = props.width || "100%";
    const iconUrl = props.iconUrl || "";
    const textColor = props.textColor || 'white';
	const backColor = props.backColor || '#CE5454';
	const unclickableBackColor = props.textColor || 'rgba(206, 84, 84, 0.1)';                 // Gold as default value
    const fontSize = props.fontSize? `${props.fontSize * scale}px`: `${20 * scale}px`; // 20px as default value
    const fontWeight = props.fontWeight || 700;  // bold as default value
    const type = props.type || 'button';
    
    return (
        <button 
            className='l-button'
            onClick={() => props.do()}
            type={type}
            style={{
                width: width,
				padding: `${8 * scale}px ${24 * scale}px`,
				background: clickable? backColor: unclickableBackColor,
            }}
        >
            <label
                style={{
                    fontSize: fontSize,
                    fontWeight: fontWeight,
                    color: textColor,
                }}
                className='h-center'
                data-text-color='white'
            >
                <img 
                    src={iconUrl}
                    className="c-button__icon"
                    alt="button"
                />
				<span>
					{props.name}
				</span>
            </label>
        </button>
    );
}

export default Button