import React from 'react';
import "./style.scss";

import {
    Gap
} from "components";

type ModalProps = {
    title: string
    message: string,
}

const Modal: React.FC<ModalProps> = ({
    title,
    message,
}: ModalProps) => {
    return (
        <div className="l-modal">
            <img className="c-modal__close-icon" src="/assets/icons/close.svg" />
            <h2>
                {title}
            </h2>
            <Gap gap={24} axis="vertical" />
            <p className="c-modal__message">
                {message}
            </p>
        </div>
    )
}

export default Modal;