import React, { useState, useContext, useEffect } from 'react';
import { Ref, FieldError } from 'react-hook-form/dist/types';
import './style.scss';

import {
    Gap
} from 'components';
import Modal from "./components/Modal";
import AppContext, { AppContextInterface } from 'contexts/AppContext';

function charPattern(type: string) {
    let pattern;
    switch (type) {
        case "phone":
            pattern = /[+0-9]/
            break;
        case "number":
            pattern = /[0-9]/
            break;
        default:
            pattern = /.*/
            break;
    }

    return pattern;
}

interface FieldProps {
    name: string,
    type: string,
    placeholder?: string,
    scale: number,
    info?: string,
    editable?: boolean
    updateValue: (value: any) => void,
    dropdownList?: any[][],
    formRef: Ref,
    error: FieldError | undefined,
}

const doNothing = () => {}

const Field: React.FC<FieldProps> = (props: FieldProps) => {
    const appContext = useContext<AppContextInterface>(AppContext);
    const editable = props.editable === undefined? true: props.editable;
    const [value, setValue] = useState("");
    const [inputOnFocus, setInputOnFocus] = useState(false);
    const scale = props.scale || 1;
    const type: string = props.type || "text";
    const filterKey: any = (e: KeyboardEvent) => {
        const key = String.fromCharCode(e.which)
        if (!key.match(charPattern(type))) {
            return false;
        }
    }

    const updateValue = (newValue: any, sentValue?: any) => {
        if (sentValue === undefined || sentValue === null) {
            props.updateValue(newValue);
            setValue(newValue)
        } else {
            props.updateValue(sentValue);
            setValue(newValue);
        }
    }

    const setFocusWhenDropdownExists = () => setInputOnFocus(!inputOnFocus);
    
    const setValueAfterChange = (e: React.ChangeEvent<HTMLInputElement>) => updateValue(e.target.value);

    useEffect(() => {
        if (props.dropdownList !== undefined) {
            updateValue(props.dropdownList[0][1], props.dropdownList[0][0]);
        }
    }, []);

    return (
        <>
            <div className="l-field__name">
                <span className="c-field__name">
                    {props.name}
                </span>
                <Gap gap={8} axis="horizontal" />
                <button
                    type="button"
                    className="c-field__help h-center"
                    onClick={() => appContext.showModal(
                        <Modal 
                            title={props.name}
                            message={props.info || "Not available"}
                        />
                    )}
                >
                    ?
                </button>
            </div>
            <Gap gap={8} axis="vertical" />
            <div className="l-field__value">
                <input 
                    className="c-field__value h-animation"
                    type={type}
                    placeholder={props.placeholder || ""}
                    onKeyPress={filterKey}
                    value={value}
                    ref={props.formRef}
                    onChange={setValueAfterChange}
                    onClick={!!props.dropdownList? setFocusWhenDropdownExists: doNothing}
                    readOnly={!editable || !!props.dropdownList}
                    data-editable={editable}
                    data-state={inputOnFocus? "focus": "idle"}
                    style={{
                        padding: `0 ${14 * scale}px`,
                        height: `${40 * scale}px`,
                    }}
                    name={props.name}
                />
                <div className="c-field__bottom-line h-animation" />
                {editable && props.dropdownList
                    ? <div
                        className="l-field__dropdown h-animation"
                        style={{
                            padding: `8px ${14 * scale}px`,
                        }}
                    >
                        {props.dropdownList!.map((item: any, index: number) => {
                            const sendDropdownItem = () => {
                                setInputOnFocus(false);
                                updateValue(item[1], item[0]);
                            }

                            const components = [
                                <span
                                    key={2 * index}
                                    className="c-field__dropdown-item h-animation"
                                    onClick={sendDropdownItem}
                                >
                                    {item[1]}
                                </span>
                            ]

                            if (index + 1 < Object.keys(props.dropdownList!).length) {
                                components.push(<Gap key={2 * index + 1} gap={8} axis="vertical" />);
                            }
                            return components;
                        })}
                    </div>
                    : <span />
                }
                {props.dropdownList
                    ? <img src="/assets/icons/arrow-down.svg" className="c-field__arrow-down-icon" />
                    : <span />
                }
            </div>
            { props.error && <span>{props.error.message || 'Error on input'}</span> }
        </>
    );
}

export default Field;
