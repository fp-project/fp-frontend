import React, { useState } from 'react';
import "./style.scss";

import {
    Button
} from "components"; 


type TabBarProps = {
    titles: string[],
    children: any,
}

const TabBar: React.FC<TabBarProps> = ({
    titles,
    children
}: TabBarProps) => {
    const [currentIndex, setCurrentIndex] = useState(0);
    return (
        <div className="l-tab-bar">
            <div className="l-tab-bar__buttons">
                {titles.map((title, index) => {
                    return (
                        <Button
                            width="100%"
                            key={index}
                            name={title}
                            textColor={currentIndex !== index? "rgba(238, 238, 238, 0.3)": undefined}
                            backColor={currentIndex !== index? "rgba(206, 84, 84, 0.1)": undefined}
                            do={() => setCurrentIndex(index)}
                        />
                    )
                })}
            </div>
            {children.length === 1? children: children[currentIndex]}
        </div>
    )
}

export default TabBar;